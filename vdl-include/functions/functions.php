<?php

function doLogin() {
	$LOGIN = new LOGIN;
	$sucess= $LOGIN->start($_POST['email'],$_POST['password']);
	if($sucess != false){
		//Recuperamos datos del usuario
		$USER = new USER();
		$USER->load_user($sucess["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
		$datos = $USER->firstLoad();
		//Generamos token de sesion
		$TOKEN = new TOKEN();
		$token = $TOKEN->gen_token($_POST['email'], $_POST['ip'], $_POST['remember'],$sucess["id"]); //recibir tambien el id del usuario como comprobacion
		//Añadimos el token al array asociativo que vamos a devolver
		$datos["token"] = $token;
		$datos["id"] = $sucess["id"];
		//$auth_session = array("session_auth" => "test");
		echo json_encode($datos);
	}
	else
		return false;
}

function getLastUpdates() {
	$UPDATE = new UPDATE();
		
	$datos = $UPDATE->getLastUpdates($_POST['token'], $_POST['num']);
	
	echo json_encode($datos);
}

function recover($email){
	$USER=new USER();
	$success = $USER->recover_password();
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function getFriendUpdate($id){
	
}


function getChats($id){
	$CHAT = new CHAT();
	
	$chats = $CHAT->get_chats($id);
	
	$MESSAGE = new MESSAGE();
	$mensajes = array();
	
	foreach ($chats as $elemento) {
		array_push($mensajes, array($elemento, $MESSAGE->get_messages($elemento)));
	}
	
	echo json_encode($mensajes);
}

function setChat($participantes){
	$CHAT = new CHAT();
		
	//$participantes= array(1, 2, 3, 4);
		
	$datos = $CHAT->create_chat();
	
	$valor = false;
	
	if ($datos >= 0) {
	
		$valor = true;
		
		foreach ($participantes as $elemento) {
			if ($valor == true)
				$valor = $CHAT->set_user_chat($datos, $elemento);
		}
		
	}
	
	echo json_encode($valor);	
}

function setMessage($id_conver,$id_user,$text){
	$MESSAGE = new MESSAGE();
	
	$valor = $MESSAGE->set_messages($id_conver, $id_user, $text);
	
	echo json_encode($valor);
}

?>