<?php

function addRoute(){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$ROUTE = new ROUTES();
	$stops = json_decode($item->jdata);
	$datos = $ROUTE->add_route($item->id_user,$item->name,$item->starttime,$item->endtime,$item->days,$item->jdata);
	if($datos != false){
		$datos1 = $ROUTE->add_stops($stops,$datos,$item->id_user);
		$datos = $ROUTE->add_trace($item->coordinates,$datos,$item->id_user);
	}
//	if(isset($item->price) && isset($item->seats))
//		$datos = $ROUTE->add_carpool($item->id_user,$item->price, $item->seats);
	echo json_encode($datos);
}

function getRoutes($id){
	$ROUTE = new ROUTES();
	$datos = $ROUTE->get_routes($id);
	//$UPDATE = new UPDATE();
	//$datos = $UPDATE->addUserUpdate($item->token, $item->text,$item->lat, $item->lon);
	array_walk_recursive($datos, function(&$value, $key) {
    	if (is_string($value)) {
        	$value = utf8_encode ( $value );
    	}
	});
	echo json_encode($datos);
}

function deleteRoute($id){
	$ROUTE = new ROUTES();
	$datos = $ROUTE->delete_route($id);
	//$UPDATE = new UPDATE();
	//$datos = $UPDATE->addUserUpdate($item->token, $item->text,$item->lat, $item->lon);
	echo json_encode($datos);
}

function searchroute($id,$value,$latitude,$longitude){
	$ROUTE = new ROUTES();
	$datos = $ROUTE->search_route($id,$value,$latitude,$longitude);
	array_walk_recursive($datos, function(&$value, $key) {
    	if (is_string($value)) {
        	$value = utf8_encode ( $value );
    	}
	});
	echo json_encode($datos);
}

function addVehicle(){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$ROUTE = new ROUTES();
	$type = $item->type;
	if($item->usevehicle == "false")
		$type = "foot";
	$datos = $ROUTE->add_vehicle($item->id_user,$type,$item->model, 
								 $item->n_places, $item->fuel_capacity, $item->hideroutes);
	if(isset($item->nameservice) && isset($item->typeservice) && isset($item->fare))
		$datos = $ROUTE->add_service($item->id_user,$item->nameservice,$item->typeservice, $item->fare);
	echo json_encode($datos);
}

function getVehicles($id){
	$ROUTE = new ROUTES();
	$datos = $ROUTE->get_vehicle($id);
	echo json_encode($datos);
}

?>