<?php

function getTokens() {
	//echo json_encode('token:1,token:2');
	//Recuperamos datos del usuario
		$USER = new USER("test@test.com");
		$datos = $USER->firstLoad();
		
		//Generamos token de sesion
		$TOKEN = new TOKEN();
		$token = $TOKEN->gen_token("1", "192.168.1.33", "no");
		
		//Añadimos el token al array asociativo que vamos a devolver
		$datos["token"] = $token;
		
		//$auth_session = array("session_auth" => "test");
		echo json_encode($datos);
}
 
function getToken($id) {
	if($id == 'vidaliapp'){
		$arr = array('vdl_utoken' => md5('token_example')."-".crypt("public_key"));
		echo json_encode($arr);
	}
}

function getUpdates($id){
	$UPDATE = new UPDATE();
	$result = $UPDATE->getwallupdates($id);
	array_walk_recursive($result, function(&$value, $key) {
    	if (is_string($value)) {
        	$value = utf8_encode ( $value );
    	}
	});
	echo json_encode($result);
	return true;
}

function setUserData() {
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$USER = new USER();
	$sucess= $USER->updateuser($item->id_user,$item->email,$item->nick,$item->name,$item->sex,$item->birthdate,
							$item->default_location,$item->place,$item->description,$item->website,
							$item->privacy_level);
	if($sucess == true){		
		echo json_encode("{message: '$sucess'}");
	}
	else
		return "fallo insertado";
}


function setUser2() {
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$USER = new USER();
	$sucess= $USER->create($item->email,$item->nick,$item->password,$item->name,$item->sex,$item->birthdate,
							$item->default_location,$item->place,"img_default",$item->description,$item->website,
							$item->privacy_level,$item->view_mode,$item->hide_mode,$item->mail_notify,
							$item->n_contacts,$item->n_groups);
	if($sucess == true){		
		$LOGIN = new LOGIN;
		$loged= $LOGIN->start($item->email,$item->password);
		if($loged != false){
			//Recuperamos datos del usuario
			$USER = new USER();
			$USER->load_user($loged["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
			$datos = $USER->firstLoad();
			//Generamos token de sesion
			$TOKEN = new TOKEN();
			$token = $TOKEN->gen_token($item->email,$item->ip,$item->remember,$loged["id"]); //recibir tambien el id del usuario como comprobacion
			//Añadimos el token al array asociativo que vamos a devolver
			$datos["token"] = $token;
			$datos["id"] = $loged["id"];
			//$auth_session = array("session_auth" => "test");
			echo json_encode($datos);
		}
		else
			echo "fallo inicio";
	}
	else
		return "fallo insertado";
}

function getUserData($id){
	
}

function getUserPosts($user){
	$UPDATE = new UPDATE();
	$result = $UPDATE->getupdates($user,10);
	echo json_encode($result);
	return true;
}

function addUpdate($user){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$UPDATE = new UPDATE();
	$datos = $UPDATE->addUserUpdate($item->token, $item->text,$item->lat, $item->lon);
	echo json_encode($datos);
}

function deletePost($user, $post){
		
	$UPDATE = new UPDATE();
	
	$success = $UPDATE->deleteUserUpdate($post);
	
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function addFriend($user, $friend){
	$USER=new USER();
	$success = $USER->add_friends($user, $friend);
	if($success){
		$NOTIFY = new NOTIFY();
		$NOTIFY->notify_friend($user, $friend);
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function setFriend($user, $friend){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	//$user2 = $item->id_user;
	//$user1 = $item->id_creator;
	$notification = $item->id_notification;
	$status ="confirmed";
	$USER=new USER();
	$success = $USER->set_friends($user, $friend, $status);
	if($success){
		$NOTIFY = new NOTIFY();
		$done = $NOTIFY->mark_read($notification);
		if($done){
			echo json_encode($success);
		}
	}
	else{
		return false;
	}
}

function deleteFriend($user, $friend){
	$USER = new USER();
	$success = $USER->delete_friend($user, $friend);
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function getGroups($id){
	$GROUP = new GROUP();
		
	$datos = $GROUP->getUserGroups($id);
	
	//$datos = $GROUP->getLastUserGroups("e06dcd16c89cab196b4a974582a6b8130c8375b4", "10");
	
	echo json_encode($datos);
}

function joinGroup ($user){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$GROUP = new GROUP();
	$datos = $GROUP->joinGroup($user, $item->group);
	echo json_encode($datos);
}

function leaveGroup ($user, $group){
	/*$pos = strpos($id, '#', 0);
	$userid = substr($id, 0, $pos);
	$group = substr($id, $pos + 1, strlen($id));
	$group = str_replace('~', ' ', $group);*/
	$GROUP = new GROUP();
	$datos = $GROUP->leaveGroup($user, $group);
	echo json_encode($datos);
}

function getNotifications($id) {
	$NOTIFY = new NOTIFY();
	$datos = $NOTIFY->getUserNotify($id);	
	echo json_encode($datos);
}

?>