<?php
require_once 'CORE_MAIN.php';


/**
 * class NOTIFY
 * 
 */
class ROUTES extends CORE_MAIN
{

	public function add_carpool( $id_user,$car_id, $price, $seats ) { // antes debería crear el coche. limitar esto si no tiene coche fijado
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_user_routes_pool(`user_id`, `car_id`, `price`)
				VALUES ('$id_user', '$car_id','$price')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends


	public function add_route( $id_user, $name,$starttime,$endtime,$days,$data) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_user_routes(`id_user`, `name`,`time_departure`,`time_arrival`,`days_route`,`data`)
				VALUES ('$id_user', '$name','$starttime','$endtime','$days','$data')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return $connection->insert_id;
		}
	} // end of member function notify_friends

	public function add_trace($data,$routeid) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_routes_trace(`route_related`, `lat`,`lon`) VALUES";
		$i = 0;
		$len = count($data);
		foreach ($data as $value) {
			$query .= "('$routeid', '".$value[0]."','".$value[1]."'),";
			if($i == ($len -1)){
				$query .= "('$routeid', '".$value[0]."','".$value[1]."')";				
			}
			$i++;
		}
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends


	public function add_stops($data,$routeid,$userid) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_routes_stops(`stop_point`, `stop_name`,`route_related`,`user_related`) VALUES";
		$i = 0;
		$len = count($data);
		foreach ($data as $value) {
			if($i == 0){
				$query2 = "UPDATE vdl_user_routes SET start_point_lat='".$value->latLng->lat."', start_point_lon='".$value->latLng->lng."' WHERE id='$routeid'";
				$res = $connection->query($query2);
				if($res != 1){
					$message  = 'Invalid query: ' . $connection->error . "\n";
					$message .= 'Whole query: ' . $query;
					die($message);
				}
			}
			if($i < $len - 1){
				$query .= "('".serialize($value->latLng)."', '".$value->name."','$routeid','$userid'),";
			}else{
				$query2 = "UPDATE vdl_user_routes SET end_point_lat='".$value->latLng->lat."', end_point_lon='".$value->latLng->lng."' WHERE id='$routeid'";
				$query .= "('".serialize($value->latLng)."', '".$value->name."','$routeid','$userid')";
				$res = $connection->query($query2);
				if($res != 1){
					$message  = 'Invalid query: ' . $connection->error . "\n";
					$message .= 'Whole query: ' . $query;
					die($message);
				}
			}
			$i++;
		}
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends

	public function get_routes( $id_user) {
  		$connection = parent::connect();
		$query = "SELECT *
		 	 	 FROM vdl_user_routes  
		 	 	 WHERE id_user = '$id_user'";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			$arresult = array();
			while ($row = $data->fetch_assoc()) {
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function notify_message

		public function delete_route( $id_user) {
  		$connection = parent::connect();
		$query = "DELETE FROM `vidali1`.`vdl_user_routes` WHERE `vdl_user_routes`.`id` = $id_user";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
	} // end of member function notify_message

	public function add_vehicle($id_user,$vehicletype,$vehiclemodel, $nseats, $fuelcapacity, $hideroutes) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_routes_vehicle(`id_owner`, `n_places`, `type`, `model`, `fuel_capacity`)
				VALUES ('$id_user', '$nseats', '$vehicletype','$vehiclemodel','$fuelcapacity')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends

	public function add_service($id,$name,$type,$fare) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_routes_private_services(`user_id`, `name`, `service_type`, `fare`)
				VALUES ('$id','$name', '$type', '$fare')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends

	public function update_vehicle($id,$vehicletype,$vehiclemodel, $nseats, $fuelcapacity, $hideroutes) {
  		$connection = parent::connect();
		$query = "UPDATE vdl_routes_vehicle SET `n_places`='$nseats', `type`='$vehicletype',
												`model`='$vehiclemodel', `fuel_capacity`='$fuelcapacity'
				WHERE id=$id";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends


	public function get_vehicle( $id_user) {
  		$connection = parent::connect();
		$query = "SELECT A.id AS id_vehicle, A.user_id, A.name, A.service_type, A.fare, 
				  B.id AS id_service, B.n_places, B.type, B.model, B.fuel_capacity 
				  FROM `vdl_routes_private_services` A JOIN vdl_routes_vehicle B ON A.user_id = B.id_owner 
				  WHERE A.user_id ='$id_user'";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			$arresult = array();
			while ($row = $data->fetch_assoc()) {
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function notify_message

	protected function get_location($data){
		$request=file_get_contents("http://nominatim.openstreetmap.org/search?q=".$data."&format=json&addressdetails=0&limit=1");
		$json=json_decode($request, true);
		$results = ["lat" => $json["lat"], "lon" => $json["lon"]];
		return $results;
	}


	protected function calc_distance($lat1,$lat2,$lon1,$lon2){
		$results=array();
		$request=file_get_contents("http://router.project-osrm.org/table?loc=".$lat1.",".$lon1."&loc=".$lat2.",".$lon2);
		$json=json_decode($request, true);
		$results = $json["distance_table"];
		return $results;
	}

	public function search_route($id_user, $value, $latitude, $longitude) {
  		$connection = parent::connect();
  		$dest = $this->get_location($value);
		$query = "SELECT *
		 	 	 FROM vdl_user_routes  
		 	 	 WHERE ( LOWER(data) REGEXP '.*".$value.".*') OR  
		 	 	 ( (start_point_lat > $latitude - 0.005) AND 
		 	 	 (start_point_lat <= $latitude + 0.005) AND 
		 	 	 (start_point_lon <= $longitude + 0.005) AND 
		 	 	 (start_point_lon > $longitude - 0.005) OR
		 	 	  (end_point_lat > ".$dest["lat"]." - 0.005) AND 
		 	 	 (end_point_lat <= ".$dest["lat"]." + 0.005) AND 
		 	 	 (end_point_lon <= ".$dest["lon"]." + 0.005) AND 
		 	 	 (end_point_lon > ".$dest["lon"]." - 0.005))";
		$data=$connection->query($query); //Aqui tengo ya las rutas con las paradas con nombres similares, cercanas al punto de salida del usuario
		//o cercanas al punto de llegada del mismo

		//Falta coger las rutas que vayan cerca del usuario, que no tengan paradas.
		if(!$data){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			$matrix = array();
			$arresult = array();
			while ($row = $data->fetch_assoc()) {
				//obtener la matrix de distancia entre el punto de salida y los puntos cercanos de inicio
				array_push($matrix, $this->calc_distance($latitude,$longitude,$row["start_point_lat"],$row["start_point_lon"]));
				//obtener la matrix de distancia entre el punto de destino y los puntos cercanos de final

				//obtener la matrix de distancia entre los puntos de una ruta y los puntos de inicio.

				//Aplicar A* para determinar cuales son los mas cercanos
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function notify_message

} // end of NOTIFY
?>