<?php

	include "vdl-include/class/CORE_MAIN.php";
	foreach (glob("vdl-include/class/*.php") as $filename) {
		include_once $filename;
	}

	foreach (glob("vdl-include/functions/*.php") as $filename) {
		include_once $filename;
	}

	require 'vdl-include/Slim/Slim.php';
	\Slim\Slim::registerAutoloader();

	session_start();
	ini_set('mssql.charset', 'UTF-8');

	$app = new \Slim\Slim();

	//------ USUARIOS --------
	$app->get('/tokens(/)', function () use($app) {
		//echo json_encode('token:1,token:2');
		//Recuperamos datos del usuario
		$USER = new USER("test@test.com");
		$datos = $USER->firstLoad();
		
		//Generamos token de sesion
		$TOKEN = new TOKEN();
		$token = $TOKEN->gen_token("1", "192.168.1.33", "no");
		
		//Añadimos el token al array asociativo que vamos a devolver
		$datos["token"] = $token;
		
		//$auth_session = array("session_auth" => "test");
		echo json_encode($datos);
	});
	$app->get('/tokens/:id(/)', function ($token = null) use($app) {
		if($token == 'vidaliapp'){
			$arr = array('vdl_utoken' => md5('token_example')."-".crypt("public_key"));
			echo json_encode($arr);
		}
	});

	//$app->get('/users/', '');
	//$app->delete('/users/:id', '');
	$app->get('/users/:id(/)', function ($user = null) use($app) {
		
	});
	$app->post('/users(/)', function () use($app) {
		$rawJSONString = file_get_contents('php://input');
		$item = json_decode($rawJSONString);
		$USER = new USER();
		$sucess= $USER->create($item->email,$item->nick,$item->password,$item->name,$item->sex,$item->birthdate,
								$item->default_location,$item->place,"img_default",$item->description,$item->website,
								$item->privacy_level,$item->view_mode,$item->hide_mode,$item->mail_notify,
								$item->n_contacts,$item->n_groups);
		if($sucess == true){		
			$LOGIN = new LOGIN;
			$loged= $LOGIN->start($item->email,$item->password);
			if($loged != false){
				//Recuperamos datos del usuario
				$USER = new USER();
				$USER->load_user($loged["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
				$datos = $USER->firstLoad();
				//Generamos token de sesion
				$TOKEN = new TOKEN();
				$token = $TOKEN->gen_token($item->email,$item->ip,$item->remember,$loged["id"]); //recibir tambien el id del usuario como comprobacion
				//Añadimos el token al array asociativo que vamos a devolver
				$datos["token"] = $token;
				$datos["id"] = $loged["id"];
				$app->status(201);
				$app->contentType('application/json');
				//$auth_session = array("session_auth" => "test");
				echo json_encode($datos);
			}
			else {
				$app->status(500);
				$app->contentType('application/json');
				$datos['message'] = 'Error al iniciar sesión';
				echo json_encode($datos);
			}
		}
		else {
			$app->status(400);
			$app->contentType('application/json');
			$datos['message'] = 'Error al insertar nuevo usuario';
			echo json_encode($datos);
		}
	});
	$app->put('/users/:id(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$rawJSONString = file_get_contents('php://input');
			$item = json_decode($rawJSONString);
			$USER = new USER();
			$sucess= $USER->updateuser($user,$item->email,$item->nick,$item->name,$item->sex,$item->birthdate,
									$item->default_location,$item->place,$item->description,$item->website,
									$item->privacy_level);
			if($sucess == true){
				$app->status(200);
				$app->contentType('application/json');
				$datos['message'] = 'Usuario actualizado correctamente.';
				echo json_encode($datos);
			}
			else {
				$app->status(500);
				$app->contentType('application/json');
				$datos['message'] = 'Error al actualizar el usuario.';
				echo json_encode($datos);
			}
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});

	$app->get('/users/:id/wall(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$UPDATE = new UPDATE();
			$result = $UPDATE->getwallupdates($id);
			array_walk_recursive($result, function(&$value, $key) {
				if (is_string($value)) {
					$value = utf8_encode ( $value );
				}
			});
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($result);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});

	//Posts de usuario
	//$app->put('/users/:user/post/:post', '');
	$app->get('/users/:user/posts(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$UPDATE = new UPDATE();
			$result = $UPDATE->getupdates($user,10);
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($result);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	$app->post('/users/:user/posts(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$rawJSONString = file_get_contents('php://input');
			$item = json_decode($rawJSONString);
			$UPDATE = new UPDATE();
			$datos = $UPDATE->addUserUpdate($item->token, $item->text,$item->lat, $item->lon);
			$app->status(201);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	$app->delete('/users/:user/posts/:post(/)', function ($user = null, $post = null) use($app) {
		if (is_numeric($user) and is_numeric($post)){
			$UPDATE = new UPDATE();
			$success = $UPDATE->deleteUserUpdate($post);			
			if($success){
				$app->status(204);
				$app->contentType('application/json');
				echo json_encode($success);
			}
			else{
				$datos["success"] = 0;
				$datos["message"] = "Error al eliminar el post.";
				$app->status(500);
				$app->contentType('application/json');
				echo json_encode($datos);
			}
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});

	//Friends de usuario
	//$app->get('/users/:user/friends', '');
	$app->post('/users/:user/friends/:friend(/)', function ($user = null, $friend = null) use($app) {
		if (is_numeric($user) and is_numeric($friend)){
			$USER=new USER();
			$success = $USER->add_friends($user, $friend);
			if($success){
				$NOTIFY = new NOTIFY();
				$NOTIFY->notify_friend($user, $friend);
				$app->status(201);
				$app->contentType('application/json');
				echo json_encode($success);
			}
			else{
				$datos["success"] = 0;
				$datos["message"] = "Error al agregar un amigo.";
				$app->status(500);
				$app->contentType('application/json');
				echo json_encode($datos);
			}
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	$app->delete('/users/:user/friends/:friend(/)', function ($user = null, $friend = null) use($app) {
		if (is_numeric($user) and is_numeric($friend)){
			$USER = new USER();
			$success = $USER->delete_friend($user, $friend);
			if($success){
				$app->status(204);
				$app->contentType('application/json');
				echo json_encode($success);
			}
			else{
				$datos["success"] = 0;
				$datos["message"] = "Error al eliminar amigo.";
				$app->status(500);
				$app->contentType('application/json');
				echo json_encode($datos);
			}
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	//$app->post('/contact','setFriend'); pasa a ser:
	$app->put('/users/:user/friends/:friend(/)', function ($user = null, $friend = null) use($app) {
		if (is_numeric($user) and is_numeric($friend)){
			$rawJSONString = file_get_contents('php://input');
			$item = json_decode($rawJSONString);
			//$user2 = $item->id_user;
			//$user1 = $item->id_creator;
			$notification = $item->id_notification;
			$status ="confirmed";
			$USER=new USER();
			$success = $USER->set_friends($user, $friend, $status);
			if($success){
				$NOTIFY = new NOTIFY();
				$done = $NOTIFY->mark_read($notification);
				if($done){
					$app->status(200);
					$app->contentType('application/json');
					echo json_encode($success);
				}
			}
			else{
				$datos["success"] = 0;
				$datos["message"] = "Error al actualizar el estado de amistad.";
				$app->status(500);
				$app->contentType('application/json');
				echo json_encode($datos);
			}
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});

	//Groups de usuario
	$app->get('/users/:user/groups(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$GROUP = new GROUP();		
			$datos = $GROUP->getUserGroups($id);
			//$datos = $GROUP->getLastUserGroups("e06dcd16c89cab196b4a974582a6b8130c8375b4", "10");
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	$app->post('/users/:user/groups(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$rawJSONString = file_get_contents('php://input');
			$item = json_decode($rawJSONString);
			$GROUP = new GROUP();
			$datos = $GROUP->joinGroup($user, $item->group);
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});
	$app->delete('/users/:user/groups/:group(/)', function ($user = null, $group = null) use($app) {
		if (is_numeric($user) and is_numeric($group)){
			/*$pos = strpos($id, '#', 0);
			$userid = substr($id, 0, $pos);
			$group = substr($id, $pos + 1, strlen($id));
			$group = str_replace('~', ' ', $group);*/
			$GROUP = new GROUP();
			$datos = $GROUP->leaveGroup($user, $group);
			$app->status(204);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});

	//Notificaciones de usuario
	$app->get('/users/:id/notification(/)', function ($user = null) use($app) {
		if (is_numeric($user)){
			$NOTIFY = new NOTIFY();
			$datos = $NOTIFY->getUserNotify($id);	
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});


	//------ FUNCIONALIDADES --------
	$app->post('/login(/)', function () use($app) {
		$LOGIN = new LOGIN;
		$sucess= $LOGIN->start($_POST['email'],$_POST['password']);
		if($sucess != false){
			//Recuperamos datos del usuario
			$USER = new USER();
			$USER->load_user($sucess["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
			$datos = $USER->firstLoad();
			//Generamos token de sesion
			$TOKEN = new TOKEN();
			$token = $TOKEN->gen_token($_POST['email'], $_POST['ip'], $_POST['remember'],$sucess["id"]); //recibir tambien el id del usuario como comprobacion
			//Añadimos el token al array asociativo que vamos a devolver
			$datos["token"] = $token;
			$datos["id"] = $sucess["id"];
			//$auth_session = array("session_auth" => "test");
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$app->status(401);
			$app->contentType('application/json');
			$datos['message'] = 'Error al iniciar sesión.';
			echo json_encode($datos);
		}
	});
	/*$app->post('/lastUpdates', 'getLastUpdates');
	$app->get('/recover/:email', 'recover');	//post de email? 
	$app->get('/update/friends/:id', 'getFriendUpdate');

	$app->post('/setchat/:participantes', 'setChat');
	$app->get('/chat/:id', 'getChats');
	$app->post('/setmessage/:contenido', 'setMessage');*/


	//------ GRUPOS --------
	$app->get('/getallgroups/:id(/)', function ($id = null) use($app) {
		if (is_numeric($id)){
			$GROUP = new GROUP();
			$datos = $GROUP->getUserGroups($id);
			$datos2 = $GROUP->getNoUserGroups($id);
			foreach ($datos2 as $dato) {
				array_push($datos, $dato);
			}
			array_walk_recursive($datos, function(&$value, $key) {
				if (is_string($value)) {
					$value = utf8_encode ( $value );
				}
			});
			//$datos = $GROUP->getLastUserGroups("e06dcd16c89cab196b4a974582a6b8130c8375b4", "10");
			$app->status(200);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
		else {
			$datos["success"] = 0;
			$datos["message"] = "El id de usuario debe ser numérico.";
			$app->status(400);
			$app->contentType('application/json');
			echo json_encode($datos);
		}
	});


	//------ RUTAS --------
	$app->post('/routes/add', 'addRoute');
	$app->get('/routes/:id', 'getRoutes');
	$app->delete('/routes/:id', 'deleteRoute');
	$app->get('/search/:id/:value/:latitude/:longitude', 'searchroute');
	$app->post('/vehicle/add', 'addVehicle');
	$app->get('/vehicles/:id', 'getVehicles');
	$app->put('/vehicle/update/:id(/)', function ($id) use ($app){
		$item = json_decode($app->request()->getBody());
		$ROUTE = new ROUTES();
		$type = $item->type;
		if($item->usevehicle == "false")
			$type = "foot";
		$datos = $ROUTE->update_vehicle($id,$type,$item->model, 
									 $item->n_places, $item->fuel_capacity, $item->hideroutes);
		if(isset($item->nameservice) && isset($item->typeservice) && isset($item->fare))
			$datos = $ROUTE->add_service($item->id_user,$item->nameservice,$item->typeservice, $item->fare);
		echo json_encode($datos);
	});


	$app->run();

?>