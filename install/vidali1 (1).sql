-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-09-2014 a las 23:18:44
-- Versión del servidor: 5.5.38-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `vidali1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_chat`
--

CREATE TABLE IF NOT EXISTS `vdl_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_msg_conver_vdl_conver1` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `vdl_chat`
--

INSERT INTO `vdl_chat` (`id`, `date_created`) VALUES
(1, '2014-01-27'),
(2, '2014-01-27'),
(3, '2014-01-27'),
(4, '2014-01-27'),
(5, '2014-01-27'),
(6, '2014-01-27'),
(7, '2014-01-27'),
(8, '2014-01-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_comment`
--

CREATE TABLE IF NOT EXISTS `vdl_comment` (
  `id_user` int(11) NOT NULL,
  `id_msg_ref` int(11) NOT NULL,
  `reply` varchar(140) COLLATE utf8_bin NOT NULL,
  `date_reply` datetime NOT NULL,
  PRIMARY KEY (`id_user`,`id_msg_ref`),
  KEY `fk_vdl_comment_vdl_msg1` (`id_msg_ref`),
  KEY `fk_vdl_comment_vdl_user1` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_config`
--

CREATE TABLE IF NOT EXISTS `vdl_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `config_value` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_event`
--

CREATE TABLE IF NOT EXISTS `vdl_event` (
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_msg` int(11) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_bin NOT NULL,
  `category` set('university','business','party_spots','activism','public_places','restaurants','entertaiment','internet','television','family','fans','political','other') COLLATE utf8_bin DEFAULT NULL,
  `privacy_level` set('open','close','request') COLLATE utf8_bin DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creator` int(11) NOT NULL,
  `lat` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `long` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `event_tittle_UNIQUE` (`description`),
  KEY `fk_vdl_event_vdl_msg1` (`id_msg`),
  KEY `fk_vdl_event_vdl_places1_idx` (`place_related`),
  KEY `fk_vdl_event_vdl_user1_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_event`
--

INSERT INTO `vdl_event` (`name`, `id_msg`, `description`, `category`, `privacy_level`, `start_time`, `end_time`, `place_related`, `creator`, `lat`, `long`) VALUES
('FICULL', NULL, 'Fiesta para los nuevos alumnos de la ULL', 'university,party_spots', 'request', '2013-12-28 18:00:00', '2013-12-29 04:00:00', 'Universidad de La Laguna', 2, '16', '18'),
('Gana un bono de guagua Gratis!', NULL, 'Evento de un concurso de bonos de guagua.', 'business', 'open', '2013-12-25 00:00:00', '2013-12-26 00:00:00', 'Titsa - Empresa de transporte público', 1, '16', '18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_file`
--

CREATE TABLE IF NOT EXISTS `vdl_file` (
  `id` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_msg` int(11) DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `type` set('image','audio','video','other') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_file_vdl_msg1` (`id_msg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_friend_of`
--

CREATE TABLE IF NOT EXISTS `vdl_friend_of` (
  `user1` int(11) NOT NULL,
  `user2` int(11) NOT NULL,
  `status` set('unconfirmed','confirmed','blocked') COLLATE utf8_bin NOT NULL,
  `date_status` date NOT NULL,
  PRIMARY KEY (`user1`,`user2`),
  KEY `fk_vdl_friend_of_vdl_user1` (`user1`),
  KEY `fk_vdl_friend_of_vdl_user2` (`user2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_friend_of`
--

INSERT INTO `vdl_friend_of` (`user1`, `user2`, `status`, `date_status`) VALUES
(1, 2, 'confirmed', '2013-12-16'),
(1, 3, 'confirmed', '2013-12-16'),
(1, 4, 'confirmed', '2014-02-13'),
(2, 4, 'confirmed', '2013-12-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_group`
--

CREATE TABLE IF NOT EXISTS `vdl_group` (
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `description` varchar(250) COLLATE utf8_bin NOT NULL,
  `image` varchar(45) COLLATE utf8_bin NOT NULL,
  `category` set('university','business','party_spots','activism','public_places','restaurants','entertaiment','internet','television','family','fans','political','other') COLLATE utf8_bin NOT NULL,
  `privacy_level` set('open','close','request') COLLATE utf8_bin NOT NULL,
  `n_members` int(11) NOT NULL DEFAULT '0',
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `lat` varchar(45) COLLATE utf8_bin NOT NULL,
  `long` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`name`),
  KEY `fk_vdl_group_vdl_places1_idx` (`place_related`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_group`
--

INSERT INTO `vdl_group` (`name`, `description`, `image`, `category`, `privacy_level`, `n_members`, `place_related`, `lat`, `long`) VALUES
('Breakdance LL ', 'Grupo de bboys de La Laguna, nos solemos reunir en la zona principal del intercambiador de San Benito', '', 'entertaiment', 'open', 0, 'Titsa - Intercambiador San Benito', '28.4872066', '-16.3248264'),
('Footing Santa Cruz', 'Asociación de personas que realizan footing en Santa Cruz, nuestro principal lugar de reunión es el parque la Granja', 'img_foot', 'entertaiment', 'request', 0, 'Parque La Granja', '28.463119', '-16.2657479'),
('Muevete en guagua!', 'Grupo de titsa. Promovemos el uso del transporte público a través de una campaña social totalmente adaptada a nuestro entorno.', 'img_muevete', 'business', 'open', 0, 'Titsa - Empresa de transporte público', '28.4590139', '-16.2510501'),
('Paseantes La Granja', 'Grupo de personas a las que les gusta reunirse en el parque la granja.', 'img_granja', 'public_places', 'open', 0, 'Parque La Granja', '28.463117', '-16.2957479'),
('Universidad de La Laguna', 'Grupo de la comunidad universitaria', 'img_ull', 'university', 'open', 0, 'Universidad de La Laguna', '28.4824643', '-16.3212535'),
('Vidali', 'Vidali User directory', 'img_vidali', 'internet', 'open', 0, 'Santa Cruz de Tenerife', '28.4724642', '-16.3212535');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_message`
--

CREATE TABLE IF NOT EXISTS `vdl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_send` datetime NOT NULL,
  `id_chat` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pm_msg` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`,`date_send`,`id_chat`),
  UNIQUE KEY `date_send_UNIQUE` (`date_send`),
  KEY `fk_vdl_msg_conver_vdl_user1_idx` (`user_id`),
  KEY `fk_vdl_msg_conver_vdl_conver1` (`id_chat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `vdl_message`
--

INSERT INTO `vdl_message` (`id`, `date_send`, `id_chat`, `user_id`, `pm_msg`) VALUES
(1, '2014-01-27 00:00:00', 7, 1, 'Hola que tal'),
(2, '2014-01-26 00:00:00', 8, 2, 'Hola amigos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_notify`
--

CREATE TABLE IF NOT EXISTS `vdl_notify` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_creator` int(11) NOT NULL,
  `id_receptor` int(11) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `chat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_notify_vdl_user1_idx` (`id_creator`),
  KEY `fk_vdl_notify_vdl_user2_idx` (`id_receptor`),
  KEY `fk_vdl_notify_vdl_post1_idx` (`post_id`),
  KEY `fk_vdl_notify_vdl_chat1_idx` (`chat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `vdl_notify`
--

INSERT INTO `vdl_notify` (`id`, `id_creator`, `id_receptor`, `type`, `checked`, `date`, `post_id`, `chat_id`) VALUES
(2, 2, 1, 4, 1, '2014-03-12', 11, 1),
(3, 4, 1, 4, 2, '2014-03-12', 11, 1),
(4, 3, 1, 5, 1, '2014-06-23', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_places`
--

CREATE TABLE IF NOT EXISTS `vdl_places` (
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `description` varchar(140) COLLATE utf8_bin NOT NULL,
  `type` set('public','business','other') COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `lat` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `long` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_places`
--

INSERT INTO `vdl_places` (`name`, `description`, `type`, `image`, `lat`, `long`) VALUES
('Parque La Granja', 'Parque La granja, SC de Tenerife', 'public', 'img_granja', '16', '18'),
('Santa Cruz de Tenerife', 'Ciudad de Santa Cruz de Tenerife', 'public', 'img_sc', '18', '16'),
('Titsa - Empresa de transporte público', 'Lugar donde se sitúa la entidad', 'business', 'img_titsa', '16', '18'),
('Titsa - Intercambiador San Benito', 'Intercambiado de San Benito para las paradas de Titsa', 'public', 'img_titsa_sb', '16', '18'),
('Universidad de La Laguna', 'Universidad de La Laguna', 'public', 'img_ull', '16', '18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_post`
--

CREATE TABLE IF NOT EXISTS `vdl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_group` varchar(45) COLLATE utf8_bin NOT NULL,
  `date_published` datetime NOT NULL,
  `text` varchar(140) COLLATE utf8_bin NOT NULL,
  `lat` varchar(30) COLLATE utf8_bin NOT NULL,
  `lon` varchar(30) COLLATE utf8_bin NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_msg_vdl_user1_idx` (`id_user`),
  KEY `fk_vdl_msg_vdl_group1_idx` (`id_group`),
  KEY `fk_vdl_post_vdl_places1_idx` (`place_related`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `vdl_post`
--

INSERT INTO `vdl_post` (`id`, `id_user`, `id_group`, `date_published`, `text`, `lat`, `lon`, `safe`, `place_related`) VALUES
(11, 2, 'Vidali', '2014-02-13 00:00:00', 'Hola, buenos dias', '28.475127', '-16.302409', 0, NULL),
(12, 3, 'Vidali', '2014-02-13 00:00:00', 'Esperando a la guagua', '28.4778952', '-16.3611015', 0, NULL),
(13, 4, 'Vidali', '2014-02-13 00:00:00', 'Prueba de post', '28.4728402', '-16.3471969', 0, NULL),
(14, 3, 'Vidali', '2014-02-13 00:00:00', 'Tercera prueba', '28.4834113', '-16.26057', 0, NULL),
(15, 4, 'Vidali', '2014-02-13 00:00:00', 'Detenida :)', '28.470954', '-16.3399871', 0, NULL),
(16, 1, 'Vidali', '2014-03-31 13:48:22', 'test', '28.482877199999997', '-16.322233', 0, NULL),
(17, 1, 'Vidali', '2014-03-31 14:05:27', 'test2', '28.4828554', '-16.322219999999998', 0, NULL),
(18, 1, 'Vidali', '2014-03-31 14:06:32', 'test3', '28.4828315', '-16.3222064', 0, NULL),
(19, 1, 'Vidali', '2014-06-13 10:38:51', 'test', '28.487401', '-16.315906', 0, NULL),
(20, 1, 'Vidali', '2014-06-23 10:34:41', 'Insertando Estado!', '28.471623200000003', '-16.3068713', 0, NULL),
(21, 3, 'Vidali', '2014-02-13 00:00:00', 'Prueba A', '28.4768952', '-16.3613015', 0, NULL),
(22, 4, 'Vidali', '2014-02-13 00:00:00', 'Paseando', '28.462954', '-16.3299171', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_open_sources`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_open_sources` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `id_private_service` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_routes_open_sources_vdl_routes_private_services1_idx` (`id_private_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_pool`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `group_linked` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `private_service_related` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_routes_pool_vdl_user_routes1_idx` (`user_id`),
  KEY `fk_vdl_routes_pool_vdl_routes_cars1_idx` (`car_id`),
  KEY `fk_vdl_routes_pool_vdl_group1_idx` (`group_linked`),
  KEY `fk_vdl_routes_pool_vdl_routes_private_services1_idx` (`private_service_related`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_private_services`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_private_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `service_type` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fare` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vdl_user_id_UNIQUE` (`user_id`),
  KEY `fk_vdl_routes_private_services_vdl_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `vdl_routes_private_services`
--

INSERT INTO `vdl_routes_private_services` (`id`, `user_id`, `name`, `service_type`, `fare`) VALUES
(4, 1, 'test', 'carpooling', '1'),
(5, 5, '', 'carpooling', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_stops`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_stops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stop_point` varchar(45) COLLATE utf8_bin NOT NULL,
  `stop_name` varchar(45) COLLATE utf8_bin NOT NULL,
  `route_related` int(11) NOT NULL,
  `user_related` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_routes_stops_vdl_user_routes1_idx` (`route_related`),
  KEY `fk_vdl_routes_stops_vdl_user1_idx` (`user_related`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=66 ;

--
-- Volcado de datos para la tabla `vdl_routes_stops`
--

INSERT INTO `vdl_routes_stops` (`id`, `stop_point`, `stop_name`, `route_related`, `user_related`) VALUES
(12, 'O:8:"stdClass":2:{s:3:"lat";d:28.468068817785', 'Avenida General Mola Nº 30, Avenida Islas Can', 37, 5),
(13, 'O:8:"stdClass":2:{s:3:"lat";d:28.464696465827', 'Avenida Bélgica Nº 5, Avenida de Bélgica, Isl', 37, 5),
(14, 'O:8:"stdClass":2:{s:3:"lat";d:28.464231160787', 'Plaza República Dominicana, Islas Canarias, E', 37, 5),
(15, 'O:8:"stdClass":2:{s:3:"lat";d:28.457387063112', 'DISA TRES DE MAYO, 1, Calle Ortega y Gasset, ', 37, 5),
(16, 'O:8:"stdClass":2:{s:3:"lat";d:28.470954828480', 'Escuela Universitaria de Estudios Empresarial', 37, 5),
(25, 'O:8:"stdClass":2:{s:3:"lat";d:28.480725181371', 'Comedor, Avenida Ángel Guimerá Jorge, Islas C', 42, 5),
(26, 'O:8:"stdClass":2:{s:3:"lat";d:28.455623172846', 'Entrada Barrio Buenos Aires, República Argent', 42, 5),
(27, 'O:8:"stdClass":2:{s:3:"lat";d:28.461259029166', 'CajaSiete, Avenida Benito Pérez Armas, Islas ', 42, 5),
(28, 'O:8:"stdClass":2:{s:3:"lat";d:28.467014576385', 'Calle Esmeralda Cervantes, Islas Canarias, Sa', 42, 5),
(29, 'O:8:"stdClass":2:{s:3:"lat";d:28.468068817785', 'Avenida General Mola Nº 30, Avenida Islas Can', 42, 5),
(30, 'O:8:"stdClass":2:{s:3:"lat";d:28.4715977;s:3:', 'Facultad de Ciencias Económicas y Empresarial', 43, 5),
(31, 'O:8:"stdClass":2:{s:3:"lat";d:28.481819102847', 'Universidad de La Laguna, Avenida Astrofísico', 43, 5),
(34, 'O:8:"stdClass":2:{s:3:"lat";d:28.467938499999', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 45, 1),
(35, 'O:8:"stdClass":2:{s:3:"lat";d:28.450505838034', 'I.E.S. PolitÃ©cnico Nuestra SeÃ±ora de La Can', 45, 1),
(36, 'O:8:"stdClass":2:{s:3:"lat";d:28.467876700000', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 46, 1),
(37, 'O:8:"stdClass":2:{s:3:"lat";d:28.468068817785', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 46, 1),
(38, 'O:8:"stdClass":2:{s:3:"lat";d:28.467928999999', 'Puente Zurita, Canarias, Salud-La Salle, Sant', 47, 1),
(39, 'O:8:"stdClass":2:{s:3:"lat";d:28.467728999999', 'No Sale 2, Avenida Islas Canarias, Canarias, ', 47, 1),
(40, 'O:8:"stdClass":2:{s:3:"lat";d:28.469177017489', '11, Calle Santiago Cuadrado, Canarias, Salama', 48, 1),
(41, 'O:8:"stdClass":2:{s:3:"lat";d:28.467738000000', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 48, 1),
(64, 'O:8:"stdClass":2:{s:3:"lat";d:28.467936099999', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 64, 1),
(65, 'O:8:"stdClass":2:{s:3:"lat";d:28.467842460671', 'Avenida General Mola NÂº 30, Avenida Islas Ca', 64, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_trace`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_related` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_routes_trace_vdl_user_routes1_idx` (`route_related`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=59 ;

--
-- Volcado de datos para la tabla `vdl_routes_trace`
--

INSERT INTO `vdl_routes_trace` (`id`, `route_related`, `lat`, `lon`) VALUES
(46, 64, 28.468015670776367, -16.264076232910156),
(47, 64, 28.468036651611328, -16.26427459716797),
(48, 64, 28.46803092956543, -16.264375686645508),
(49, 64, 28.46793556213379, -16.26436424255371),
(50, 64, 28.467775344848633, -16.26416015625),
(51, 64, 28.467727661132812, -16.263551712036133),
(52, 64, 28.467496871948242, -16.263587951660156),
(53, 64, 28.46735954284668, -16.262800216674805),
(54, 64, 28.46782875061035, -16.262680053710938),
(55, 64, 28.467891693115234, -16.262664794921875),
(56, 64, 28.467960357666016, -16.263513565063477),
(57, 64, 28.467824935913086, -16.26353645324707),
(58, 64, 28.467824935913086, -16.26353645324707);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_routes_vehicle`
--

CREATE TABLE IF NOT EXISTS `vdl_routes_vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_owner` int(11) NOT NULL,
  `n_places` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `model` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fuel_capacity` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_owner_UNIQUE` (`id_owner`),
  KEY `fk_vdl_routes_cars_vdl_user1_idx` (`id_owner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `vdl_routes_vehicle`
--

INSERT INTO `vdl_routes_vehicle` (`id`, `id_owner`, `n_places`, `type`, `model`, `fuel_capacity`) VALUES
(1, 1, '5', 'car', 'Renault Clio', '35'),
(2, 7, '', 'foot', '', ''),
(3, 5, '4', 'car', 'Ferrari Enzo', '60');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_token`
--

CREATE TABLE IF NOT EXISTS `vdl_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(45) COLLATE utf8_bin NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL,
  `expiration_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`user_id`),
  UNIQUE KEY `token` (`token`),
  KEY `fk_vdl_token_vdl_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=126 ;

--
-- Volcado de datos para la tabla `vdl_token`
--

INSERT INTO `vdl_token` (`id`, `user_id`, `token`, `ip`, `expiration_time`) VALUES
(3, 1, 'e06dcd16c89cab196b4a974582a6b8130c8375b4', '10.220.32.244, ', '2013-12-16 11:49:12'),
(4, 1, '40f1ac0455d78f5cbde2146d1c2debeea9f9492d', '10.220.32.244, ', '2013-12-16 11:49:59'),
(5, 1, '73b579e9eade9f5e6179c427d4d148148293d1d5', '10.220.32.244, ', '2013-12-16 12:08:10'),
(6, 1, '4276fa08dd82bee56f43f9f4e0df384793e72c46', '10.220.32.244, ', '2013-12-16 12:08:38'),
(7, 1, 'e3e61387e08b113f9813a355d632c689bc470060', '10.220.32.244, ', '2013-12-16 12:15:22'),
(8, 1, '64777382f7f64a3c45ab8585253861a261d13c08', '10.220.32.244, ', '2013-12-16 12:17:41'),
(9, 1, '78cfba6197c5f99a2d6e986cd740dadeac4d5cbe', '10.220.32.244, ', '2013-12-16 12:19:05'),
(10, 1, 'aedfa935c968109956a06f81e607b690e569ef80', '10.220.32.244, ', '2013-12-16 12:21:07'),
(11, 1, '44cf69e2edbb38a40dd8a6c2a67080eb010c04f2', '10.220.32.244, ', '2013-12-16 12:23:36'),
(12, 1, '18644a10bfb553fbda9a40321ae4fd53838ee07f', '10.220.32.244, ', '2013-12-16 12:37:36'),
(13, 1, '62359b3649e213f0409c20599c7253942da89401', '10.220.32.244, ', '2013-12-16 12:52:22'),
(14, 1, '7f076a13baa63cdac7467b9d21e9ef6734c02b73', '10.220.32.244, ', '2013-12-16 12:53:16'),
(15, 1, 'd94f77edc35f895d709e34a8094f5d2d8367675d', '10.220.32.244, ', '2013-12-16 13:23:57'),
(16, 1, '49b867de4fc4d886d31c0b2bb56d9ea1718d348c', '10.220.32.244, ', '2013-12-16 13:24:10'),
(17, 1, '75f220503423814ae434b44f2cdade09d100686a', '10.220.32.244, ', '2013-12-16 13:24:38'),
(18, 1, '466f2799d628510e63d95b372d3f742d81357e35', '46.222.127.104', NULL),
(19, 5, 'cb124be1878f94b1bb7d0c7985fd3bf4a85b8954', '46.222.127.104', '2013-12-17 14:33:23'),
(20, 1, 'ff3e63ca5568a892411627c2086d8a9abfdbe351', '46.222.127.104', '2013-12-17 16:41:35'),
(21, 1, '4b27675d764ade69891b9048c65f310110f1ff3b', '46.222.127.104', '2013-12-17 16:52:18'),
(22, 1, '528002fb2973f5904363b638f3b649511dcb0ca0', '46.222.127.104', '2013-12-17 16:56:44'),
(23, 6, '85d3a88bf582ad6018527f9f30fd81d197d4c1b5', '193.145.124.209', '2014-02-05 17:04:55'),
(24, 1, '402846fd5e20d38775506851b5c7d8c51749e2c4', '193.145.124.209', '2014-02-05 17:05:45'),
(25, 1, 'd787799d536a6763d3d4d1f19ba8ac3e2e9d5250', '193.145.124.209', '2014-02-11 16:09:26'),
(26, 1, 'd0e4c44041b0037221d9855ef6a143e694839328', '193.145.124.209', '2014-02-11 16:19:44'),
(27, 1, '5d17728a0da0c0c37f5d7d55fe85611409085ebd', '193.145.124.209', '2014-02-11 17:43:37'),
(28, 1, '209b2bb9cb7703f778da13cb71459b88a63759c6', '193.145.124.209', '2014-02-13 12:27:34'),
(29, 1, '4ea6acd9645b2256b9dd258e8d75dec949bd849f', '193.145.124.209', '2014-02-13 15:19:57'),
(30, 1, '07fdf1d92f8bc24e275b6f9141d115e8d6bb3b97', '193.145.124.80', '2014-03-05 12:13:58'),
(31, 1, 'd8c6689bdbd8e735918bae30e232dc197abd88c1', '193.145.124.209', NULL),
(32, 1, '8a2878d428bda257468e339e75c37b2ef994eeae', '193.145.124.73', '2014-03-06 18:13:25'),
(33, 1, '93dd80864724efa5fb049680365f2500d5216136', '193.145.124.80', NULL),
(34, 1, 'bbf04a52c1cb49be83467df7c84b56b83b534d35', '193.145.124.80', NULL),
(35, 1, '609a6cf2c5ccb16ca66c01ad0f1d4a6e5b7a3b8c', '193.145.124.80', NULL),
(36, 1, '80839afbcea2412b6a7e1c00a4a2fe9c3762f6e7', '193.145.124.80', '2014-03-13 13:25:58'),
(37, 1, '0ba6e10c318b64485cf642811bb05f7c9b26cbcf', '193.145.124.80', '2014-03-13 13:28:49'),
(38, 1, '908e605d910674fba9538872dec9ba8c4c457584', '193.145.124.80', NULL),
(39, 1, '750d63f6472dbbcd700b482a0852977bf311133a', '', '2014-03-27 00:32:08'),
(40, 1, '5fc5c268017ac22a0a7c9303761dbf54eb5d63ad', '', '2014-03-30 00:58:39'),
(41, 1, '18280dd7fc6cc0a2ffa9abbad1fdb47b3eefd366', '', '2014-03-30 01:00:00'),
(42, 1, '52e0b73e7e7980dfc98a2b203c8a16fce2331b45', '193.145.124.209', NULL),
(43, 7, '6f776d4f459d4045776d5567581c6e180f6c8c2e', '193.145.124.209', '2014-03-31 13:19:33'),
(44, 7, '375f2c3a334926a23298c78494276bce27056a1c', '193.145.124.209', '2014-03-31 14:16:44'),
(45, 1, '306a9bc2b4df1a1905be11b472b74c2760838b21', '193.145.124.209', '2014-03-31 14:17:04'),
(46, 1, '9749bd578d6b5a62f469695f81ac9274cf91b95a', '193.145.124.209', '2014-03-31 14:24:11'),
(47, 1, '175bdbafe2b4c36cfb5185c0c2caa54a7eac3fff', '193.145.124.209', '2014-03-31 14:34:58'),
(48, 1, '9cc217fa875632697c829afcbf35300d7abedf5c', '193.145.124.209', '2014-03-31 14:38:23'),
(49, 1, 'a4357aea5153808ade27f9e1dcc55de3490072d5', '193.145.124.209', '2014-03-31 14:53:18'),
(50, 1, 'c32efd02c8fe38331439bda5d6c3c95a3ebdd2b8', '193.145.124.209', '2014-04-03 12:00:00'),
(51, 1, 'c8121e1cf510af3e6f92983ca6c636b28f315582', '193.145.124.209', '2014-04-03 12:02:15'),
(52, 1, 'ec646fa40f943addaffbbbe5bda9be7621714dfe', '193.145.124.209', '2014-04-03 12:07:10'),
(53, 1, 'b633d17b6ef2f703cee3c019e8afb2a24d4d8eda', '193.145.124.209', '2014-04-03 12:08:02'),
(54, 1, '5bafe32edbd17cc8b6430f4dca68c0a5c850ba5f', '193.145.124.209', NULL),
(55, 1, '590208546c774982b7a94d313eb714cfcbd1e0e1', '193.145.124.209', '2014-05-19 13:40:44'),
(56, 1, 'f206e3335b261b405aafe6482fe91faf3756a451', '193.145.124.209', '2014-05-21 14:20:47'),
(57, 1, '2b771612bc834bceaf7e24f5cf13952e2b932dce', '193.145.124.209', '2014-05-21 14:20:55'),
(58, 6, 'b247d1c6d889fd5626a8266301674091dc74fd76', '83.47.244.157', '2014-05-24 12:40:50'),
(59, 7, '4099e1b4335f70e7d02645c0e080b762f6fd3772', '83.47.244.157', '2014-05-24 12:41:57'),
(60, 7, '16a60f25d5652658a8af3fc454248a9783775712', '83.47.244.157', '2014-05-24 12:41:57'),
(61, 7, 'c83c73c0842340b91f1a70eea47ef3e8ae532c5c', '83.47.244.157', '2014-05-24 12:44:28'),
(62, 7, '7de49ac276dc2439b06c6998a7b1ac77e63547b7', '83.47.244.157', '2014-05-24 12:44:28'),
(63, 7, 'b6d3a97ab9fe9d2b5f173937d0c73af769db9abd', '83.47.244.157', '2014-05-24 12:44:28'),
(64, 7, '09b515d5fe0d66b0bf4d215b80fbb2a4c1c2fe89', '83.47.244.157', NULL),
(65, 1, '47578e589c1b729159ca808196347dfb6717b5a4', '193.145.124.80', '2014-05-26 11:17:11'),
(66, 1, '6950e40caa2d51d3c5629c05a776985e80d5a14d', '193.145.124.80', '2014-05-27 09:57:39'),
(67, 1, '38f0c76c60171df5c19e7d1869b77e66da998652', '193.145.124.80', '2014-05-27 12:21:16'),
(68, 7, '0fd1ed2d58590d12a9a054649c4fc92fe906eb59', '193.145.124.80', '2014-05-29 10:32:24'),
(69, 1, 'ffbed04fb5aa8c5ced4fa4715efbf1e74bcb434a', '193.145.124.80', '2014-05-29 10:51:24'),
(70, 1, '7682ee5506be62351c4bf37e03fdf44256186d81', '88.10.87.196', '2014-06-05 15:28:06'),
(71, 1, 'eefa35979b0248ab14d83177e72349e200bba313', '88.10.56.28', '2014-06-07 13:15:40'),
(72, 1, '84c42134672239e8fe0e946efb5d1cbc51c22823', '193.145.124.220', NULL),
(73, 1, '368f9976c9f73d29d74a0c21ffd31607663c5225', '193.145.124.80', NULL),
(74, 5, '469b241fcbb372131d7845f6ce0695bf6d15e170', '193.145.124.80', '2014-06-23 11:03:38'),
(75, 5, 'e5e2246c534535695c83562c27c059d95f20dceb', '193.145.124.80', '2014-06-23 11:05:19'),
(76, 1, '2adc475fd923a5677bcb652530af56cdadfb4093', '83.50.242.94', NULL),
(77, 1, '73f4e7935b6a290efa06f23af7da20d7af332bd7', '83.50.242.94', NULL),
(78, 1, '6392bbd1f6d88932c732d6de95777e973da070c3', '', '2014-08-20 22:28:36'),
(79, 1, 'f8ff1d4297573a2fa49567bd85d3b5c97284ad70', '', '2014-08-20 22:30:51'),
(80, 1, '061b3d2a3485402f32bd8b96047e558a8efa56c7', '', '2014-08-20 22:36:08'),
(81, 1, '54a8db22cecf378b674897225c7e42ac0ab11456', '', '2014-08-20 23:08:53'),
(82, 1, 'fd44ad49fa4e5214172b4c64c35d9b5ce748a54b', '83.50.242.94', NULL),
(83, 1, 'e6e20ecec98df10a700c1f354297f1a31e527503', '83.50.242.94', NULL),
(84, 1, '2c21f6052a8ae80fd4b4b6263b8ffa77d1a4216a', '83.50.242.94', '2014-08-21 15:01:16'),
(85, 1, '7391b9366337d643ea003ebe69287d946314ee85', '83.50.242.94', '2014-08-21 15:01:38'),
(86, 1, '408d17ecf8f8fa63ee59363cd2d2b471fcfc6d58', '83.50.242.94', '2014-08-21 15:12:57'),
(87, 1, '9d6c1914288756b44f2414d9bc399e70e81c3ec8', '83.50.242.94', '2014-08-21 15:42:06'),
(88, 1, '40ef50f69e962e0d3248e4fe73fec273cce1730d', '83.50.242.94', '2014-08-21 20:43:50'),
(89, 1, 'a358a0fbbf5a565a4367116fe1d368be3a1f3320', '', '2014-08-21 23:57:20'),
(90, 1, '915fa247121a8c2c6ed31952fd7c1e02e8316fcc', '', '2014-08-21 23:58:48'),
(91, 1, 'a82755d8ee1a6e3529573a1fe39176893808e857', '', '2014-08-22 00:03:41'),
(92, 1, '6ebced817e9dd4a10bf82161f64767ad4f2b7156', '', '2014-08-22 00:04:41'),
(93, 1, 'f2df19d4bafbe230f8c20e343f0ebb08bc2b809f', '', '2014-08-22 00:05:51'),
(94, 1, 'f941364db06e379dc507dbedf4458b577ea21957', '', '2014-08-22 00:06:51'),
(95, 1, '48be485b061e083615f44de6fcb79e2b381dde1b', '', '2014-08-22 00:07:04'),
(96, 1, '033b1aa3c8729525a546d7cb45954c954cfdb673', '', '2014-08-22 00:08:38'),
(97, 1, 'fafab6b9ba93e983c482fd8454f1052927a0b739', '', '2014-08-22 00:09:04'),
(98, 1, '471a3798e939e72dd01acd45d6d399350d492756', '', '2014-08-22 00:14:35'),
(99, 1, 'fe273c1d1c3102013f23add315d84fa5be42e2eb', '', '2014-08-22 00:20:57'),
(100, 1, '36db05feda85b224a3f209dbe651b1d83a39909e', '', '2014-08-22 00:25:44'),
(101, 1, '49def8f34fc4aa737fff5d7204666642e97b845f', '', '2014-08-22 00:43:15'),
(102, 1, 'd207dff9a70ab45ab99546449c35b11e0c2a07db', '83.50.242.94', '2014-08-22 11:55:06'),
(103, 1, 'a39ad444d6da1b1cf5e04b9c93173cc4209ffc95', '83.50.242.94', '2014-08-22 12:51:35'),
(104, 1, '2fc7b5e923f8e42adb8d2d5022e9d3e7b015c171', '83.50.242.94', '2014-08-22 13:16:53'),
(105, 1, 'f05d1923c2a1983ea452cf4a87996027389d5592', '83.50.242.94', NULL),
(106, 8, '507781f2fdcb83cad1e760ca13b48fb3a766353d', '83.50.242.94', '2014-08-24 14:30:36'),
(107, 9, '23e176104913fb2a7bc45c76aad8ab2714cea1ab', '83.50.242.94', '2014-08-24 14:33:39'),
(108, 1, '26e19637f9b52c63db3ee5696d99fe15445c2fa4', '83.50.242.94', NULL),
(109, 1, 'cd0d095ae58e095323d970f9424a7280cfe50780', '83.50.242.94', '2014-08-24 14:54:03'),
(110, 1, '9c6b5c7cd361a6f283351c157eed623395b0b03c', '83.50.242.94', '2014-08-24 14:59:55'),
(111, 1, '4cd941b83d2102c07af4c530fd12ee986f60d96e', '83.50.242.94', '2014-08-24 15:57:20'),
(112, 1, '07cb7d923b96fed277983ca3834b1b458eaf6719', '83.50.242.94', '2014-08-24 15:57:41'),
(113, 1, '589f80834ec696d2fbadf36b8896f4849188e499', '83.50.242.94', '2014-08-24 15:58:24'),
(114, 1, '752b2e9d7739644e053b5a319bffe1ac3d9405fc', '83.50.242.94', '2014-08-24 15:58:44'),
(115, 1, '6f63c99522d81827b0865765cf8f81c0d009baa2', '83.50.242.94', '2014-08-24 17:21:41'),
(116, 1, 'faaf275f9071038e853ef49e10d4ee9240df16be', '83.50.242.94', '2014-08-25 11:25:16'),
(117, 1, '786e3f066a345d9685c3b9bd1f41f37f23e841cf', '83.50.242.94', '2014-08-25 11:56:18'),
(118, 1, '90b8b16fbb2b92d3c0ba8ef4855343197915c742', '83.50.242.94', '2014-08-25 11:56:27'),
(119, 1, '1915703d5d561c68807e884c36cff261b58794e9', '83.50.242.94', NULL),
(120, 1, 'b1e8d3621c2fbe08f5764a6ca02bba94465088a6', '83.50.242.94', '2014-08-25 13:25:09'),
(121, 1, '08ffb054e56c0795cf86b8775ef436ba38ac6c43', '83.50.242.94', '2014-08-25 13:25:58'),
(122, 1, '3ef926d51399c0ef34a9bd81a82e44a4b3d2d189', '83.50.242.94', '2014-08-25 13:38:15'),
(123, 1, 'd609def0fc155073c8a6b4828d10b2a5c0710e4d', '83.50.242.94', '2014-08-25 21:51:59'),
(124, 1, 'b21271217036cfc07025f0ef57aafd766fd9cd76', '88.20.66.249', NULL),
(125, 1, 'd31a8f54d910cd6e1652b111ebff094222c57cf4', '88.20.66.249', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_tracking`
--

CREATE TABLE IF NOT EXISTS `vdl_tracking` (
  `user_id` int(11) NOT NULL,
  `lat` varchar(30) COLLATE utf8_bin NOT NULL,
  `lon` varchar(30) COLLATE utf8_bin NOT NULL,
  `time` datetime NOT NULL,
  `homespot_name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_trending`
--

CREATE TABLE IF NOT EXISTS `vdl_trending` (
  `topic` varchar(140) COLLATE utf8_bin NOT NULL,
  `count` int(10) NOT NULL,
  UNIQUE KEY `topic` (`topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user`
--

CREATE TABLE IF NOT EXISTS `vdl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) COLLATE utf8_bin NOT NULL,
  `nick` varchar(45) COLLATE utf8_bin NOT NULL,
  `password` varchar(45) COLLATE utf8_bin NOT NULL,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `sex` enum('male','female') COLLATE utf8_bin DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `default_location` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `place` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(140) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `privacy_level` set('me','friends','public') COLLATE utf8_bin DEFAULT NULL,
  `view_mode` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hide_mode` tinyint(1) DEFAULT NULL,
  `mail_notify` binary(1) DEFAULT NULL,
  `n_contacts` int(10) unsigned DEFAULT '0',
  `n_groups` int(10) unsigned DEFAULT '0',
  `session_key` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `session_id` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nick_UNIQUE` (`nick`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `vdl_user`
--

INSERT INTO `vdl_user` (`id`, `email`, `nick`, `password`, `name`, `sex`, `birthdate`, `default_location`, `place`, `image`, `description`, `website`, `privacy_level`, `view_mode`, `hide_mode`, `mail_notify`, `n_contacts`, `n_groups`, `session_key`, `session_id`) VALUES
(1, 'test@test.com', 'test', 'b80464c61eb6916aa003892772872b8092941cd5', 'Usuario A', 'male', '1990-12-20', NULL, NULL, NULL, NULL, NULL, 'friends', NULL, NULL, NULL, 0, 0, NULL, NULL),
(2, 'iradiel@vidali.com', 'iradiel', 'b80464c61eb6916aa003892772872b8092941cd5', 'Iradiel García', 'male', '1990-10-31', NULL, NULL, 'imagen_ira', 'Un chico normal, en teoría.', NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL),
(3, 'jose@vidali.com', 'jose', 'b80464c61eb6916aa003892772872b8092941cd5', 'Jose Isaac', 'male', '1990-03-19', NULL, NULL, 'img_jose', 'Un chico alto, nacionalista canario.', NULL, 'friends', NULL, NULL, NULL, 0, 0, NULL, NULL),
(4, 'haridian@vidali.com', 'haridian', 'b80464c61eb6916aa003892772872b8092941cd5', 'haridian', 'female', '1991-08-14', NULL, NULL, 'img_hari', NULL, NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL),
(5, 'cristo@vidali.com', 'cristo', 'b80464c61eb6916aa003892772872b8092941cd5', 'Cristopher Caamana', 'male', '1990-09-26', NULL, NULL, 'img_def', 'Vidali 1.0', NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL),
(6, 'test2@test.com', 'test2', 'b80464c61eb6916aa003892772872b8092941cd5', 'usuario B', 'male', '2014-06-20', '', '', '', '', '', '', '', 0, '\0', 0, 0, NULL, NULL),
(7, 'asd@asd.c', 'asd', 'b80464c61eb6916aa003892772872b8092941cd5', 'Usuario C', 'female', '2014-06-08', '', '', 'img_default', '', '', '', '', 0, '\0', 0, 0, NULL, NULL),
(8, 'test3@test.com', 'test3', '85287891ad4b8616842d84e62d6ae9f9bdae9c64', 'test3', '', '0000-00-00', '', '', 'img_default', '', '', '', '', 0, '\0', 0, 0, NULL, NULL),
(9, 'test41@test.com', 'test41', 'e712f9c21e69efe3f1566208822f3afc01d32be4', 'test41', '', '0000-00-00', '', '', 'img_default', '', '', '', '', 0, '\0', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user_data`
--

CREATE TABLE IF NOT EXISTS `vdl_user_data` (
  `phone` int(9) DEFAULT NULL,
  `mobile` int(9) DEFAULT NULL,
  `University` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Work` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Place` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group1` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group2` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group3` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group4` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group5` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`user_id`),
  KEY `fk_vdl_user_opt_vdl_group1_idx` (`University`),
  KEY `fk_vdl_user_opt_vdl_group2_idx` (`Work`),
  KEY `fk_vdl_user_data_vdl_group1_idx` (`Place`),
  KEY `fk_vdl_user_data_vdl_group2_idx` (`group1`),
  KEY `fk_vdl_user_data_vdl_group3_idx` (`group2`),
  KEY `fk_vdl_user_data_vdl_group4_idx` (`group3`),
  KEY `fk_vdl_user_data_vdl_group5_idx` (`group4`),
  KEY `fk_vdl_user_data_vdl_group6_idx` (`group5`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `vdl_user_data`
--

INSERT INTO `vdl_user_data` (`phone`, `mobile`, `University`, `Work`, `Place`, `group1`, `group2`, `group3`, `group4`, `group5`, `user_id`) VALUES
(NULL, 123456789, 'Universidad de La Laguna', 'Vidali', 'Paseantes La Granja', 'Breakdance LL ', 'Footing Santa Cruz', NULL, NULL, NULL, 1),
(987654321, NULL, 'Universidad de La Laguna', 'Muevete en guagua!', 'Breakdance LL ', 'Footing Santa Cruz', NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user_routes`
--

CREATE TABLE IF NOT EXISTS `vdl_user_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `time_departure` time DEFAULT NULL,
  `time_arrival` time DEFAULT NULL,
  `days_route` set('mon','tue','wed','thu','fri','sat','sun') COLLATE utf8_bin DEFAULT NULL,
  `transport_type` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `data` text COLLATE utf8_bin,
  `carpool_service_related` int(11) DEFAULT NULL,
  `start_point_lat` double DEFAULT NULL,
  `start_point_lon` double DEFAULT NULL,
  `end_point_lat` double DEFAULT NULL,
  `end_point_lon` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_vdl_user_routes_vdl_user1_idx` (`id_user`),
  KEY `fk_vdl_user_routes_vdl_routes_pool1_idx` (`carpool_service_related`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=65 ;

--
-- Volcado de datos para la tabla `vdl_user_routes`
--

INSERT INTO `vdl_user_routes` (`id`, `id_user`, `time_departure`, `time_arrival`, `days_route`, `transport_type`, `name`, `data`, `carpool_service_related`, `start_point_lat`, `start_point_lon`, `end_point_lat`, `end_point_lon`) VALUES
(37, 5, '08:30:00', '09:00:00', 'mon,tue,wed,thu,fri', NULL, 'Al Trabajo', '[\n  {\n    "latLng": {\n      "lat": 28.468068817785625,\n      "lng": -16.263638734817505\n    },\n    "name": "Avenida General Mola Nº 30, Avenida Islas Canarias, Islas Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.464696465827224,\n      "lng": -16.263928413391113\n    },\n    "name": "Avenida Bélgica Nº 5, Avenida de Bélgica, Islas Canarias, El Chapatal, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.464231160787595,\n      "lng": -16.26272678375244\n    },\n    "name": "Plaza República Dominicana, Islas Canarias, El Chapatal, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38003, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.457387063112037,\n      "lng": -16.264835000038147\n    },\n    "name": "DISA TRES DE MAYO, 1, Calle Ortega y Gasset, Islas Canarias, El Chapatal, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.47095482848052,\n      "lng": -16.305491924285885\n    },\n    "name": "Escuela Universitaria de Estudios Empresariales, Calle Rector Ángel M. Gutiérrez, Islas Canarias, MacKay, San Cristóbal de La Laguna, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38008, España (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(42, 5, '19:00:00', '19:30:00', 'mon,wed', NULL, 'La Laguna - Santa Cruz (Por Autopista)', '[\n  {\n    "latLng": {\n      "lat": 28.48072518137146,\n      "lng": -16.316387057304382\n    },\n    "name": "Comedor, Avenida Ángel Guimerá Jorge, Islas Canarias, Barrio Nuevo, San Cristóbal de La Laguna, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38008, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.455623172846842,\n      "lng": -16.266079545021057\n    },\n    "name": "Entrada Barrio Buenos Aires, República Argentina, Islas Canarias, Buenos Aires, Ofra-Costa Sur, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.461259029166886,\n      "lng": -16.265923976898193\n    },\n    "name": "CajaSiete, Avenida Benito Pérez Armas, Islas Canarias, El Chapatal, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.4670145763855,\n      "lng": -16.26364946365356\n    },\n    "name": "Calle Esmeralda Cervantes, Islas Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38002, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.468068817785625,\n      "lng": -16.26372456550598\n    },\n    "name": "Avenida General Mola Nº 30, Avenida Islas Canarias, Islas Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38007, España (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(43, 5, '13:00:00', '13:30:00', 'mon,wed,thu', NULL, 'Empresariales - Informática', '[\n  {\n    "latLng": {\n      "lat": 28.4715977,\n      "lng": -16.306819\n    },\n    "name": "Facultad de Ciencias Económicas y Empresariales, Calle Andrómeda, Islas Canarias, Guajara, San Cristóbal de La Laguna, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38008, España (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.481819102847485,\n      "lng": -16.321735382080078\n    },\n    "name": "Universidad de La Laguna, Avenida Astrofísico Francisco Sánchez, Islas Canarias, El Coromoto, San Cristóbal de La Laguna, Tenerife, Santa Cruz de Tenerife, Islas Canarias, 38008, España (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(45, 1, '00:00:00', '00:00:00', '', NULL, 'test', '[\n  {\n    "latLng": {\n      "lat": 28.4679385,\n      "lng": -16.2640909\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.450505838034935,\n      "lng": -16.283798217773438\n    },\n    "name": "I.E.S. PolitÃ©cnico Nuestra SeÃ±ora de La Candelaria, Calle Pedro Doblado Claverie, Canarias, San PÃ­o X, Ofra-Costa Sur, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38071, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(46, 1, '00:00:08', '00:00:09', '', NULL, 'test2', '[\n  {\n    "latLng": {\n      "lat": 28.4678767,\n      "lng": -16.264599999999998\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.468068817785625,\n      "lng": -16.263703107833862\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(47, 1, '00:00:00', '00:00:00', '', NULL, '', '[\n  {\n    "latLng": {\n      "lat": 28.467928999999998,\n      "lng": -16.2646728\n    },\n    "name": "Puente Zurita, Canarias, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.467729,\n      "lng": -16.2646728\n    },\n    "name": "No Sale 2, Avenida Islas Canarias, Canarias, Buenavista, Salud-La Salle, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(48, 1, '00:00:00', '00:00:00', '', NULL, 'asdasdadsd', '[\n  {\n    "latLng": {\n      "lat": 28.469177017489958,\n      "lng": -16.262142062187195\n    },\n    "name": "11, Calle Santiago Cuadrado, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38001, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.467738,\n      "lng": -16.2640814\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, NULL, NULL, NULL, NULL),
(64, 1, '00:00:00', '00:00:00', '', NULL, 'sssss', '[\n  {\n    "latLng": {\n      "lat": 28.4679361,\n      "lng": -16.2640936\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  },\n  {\n    "latLng": {\n      "lat": 28.467842460671896,\n      "lng": -16.263633370399475\n    },\n    "name": "Avenida General Mola NÂº 30, Avenida Islas Canarias, Canarias, Salamanca, Centro-Ifara, Santa Cruz de Tenerife, Tenerife, Provincia de Santa Cruz de Tenerife, Canarias, 38007, EspaÃ±a (mare territorial)",\n    "_initHooksCalled": true\n  }\n]', NULL, 28.46793556213379, -16.26409339904785, 28.46784210205078, -16.263633728027344);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user_routes_has_vdl_routes_open_sources`
--

CREATE TABLE IF NOT EXISTS `vdl_user_routes_has_vdl_routes_open_sources` (
  `vdl_user_routes_id` int(11) NOT NULL,
  `vdl_routes_open_sources_id` int(11) NOT NULL,
  PRIMARY KEY (`vdl_user_routes_id`,`vdl_routes_open_sources_id`),
  KEY `fk_vdl_user_routes_has_vdl_routes_open_sources_vdl_routes_o_idx` (`vdl_routes_open_sources_id`),
  KEY `fk_vdl_user_routes_has_vdl_routes_open_sources_vdl_user_rou_idx` (`vdl_user_routes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user_take_route`
--

CREATE TABLE IF NOT EXISTS `vdl_user_take_route` (
  `vdl_user_id` int(11) NOT NULL,
  `vdl_user_routes_id` int(11) NOT NULL,
  `vdl_routes_stops_id` int(11) NOT NULL,
  `time_boarding` time DEFAULT NULL,
  PRIMARY KEY (`vdl_user_id`,`vdl_user_routes_id`,`vdl_routes_stops_id`),
  KEY `fk_vdl_user_has_vdl_user_routes_vdl_user_routes1_idx` (`vdl_user_routes_id`),
  KEY `fk_vdl_user_has_vdl_user_routes_vdl_user1_idx` (`vdl_user_id`),
  KEY `fk_vdl_user_take_route_vdl_routes_stops1_idx` (`vdl_routes_stops_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_u_belong`
--

CREATE TABLE IF NOT EXISTS `vdl_u_belong` (
  `user_id` int(11) NOT NULL,
  `group_name` varchar(45) COLLATE utf8_bin NOT NULL,
  `is_admin` binary(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`group_name`),
  KEY `fk_vdl_u_belong_vdl_user1` (`user_id`),
  KEY `fk_vdl_u_belong_vdl_group1` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_u_belong`
--

INSERT INTO `vdl_u_belong` (`user_id`, `group_name`, `is_admin`, `date_joined`) VALUES
(1, 'Footing Santa Cruz', '1', '2013-12-09 00:00:00'),
(1, 'Universidad de La Laguna', '0', '2014-02-13 11:35:19'),
(1, 'Vidali', '0', '2014-02-13 11:35:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_u_conver`
--

CREATE TABLE IF NOT EXISTS `vdl_u_conver` (
  `vdl_user_id` int(11) NOT NULL,
  `vdl_msg_conver_conver_ref` int(11) NOT NULL,
  PRIMARY KEY (`vdl_user_id`,`vdl_msg_conver_conver_ref`),
  KEY `fk_vdl_user_has_vdl_msg_conver_vdl_msg_conver1_idx` (`vdl_msg_conver_conver_ref`),
  KEY `fk_vdl_user_has_vdl_msg_conver_vdl_user1_idx` (`vdl_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_u_conver`
--

INSERT INTO `vdl_u_conver` (`vdl_user_id`, `vdl_msg_conver_conver_ref`) VALUES
(1, 7),
(1, 8),
(2, 8),
(3, 8),
(4, 8);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `vdl_comment`
--
ALTER TABLE `vdl_comment`
  ADD CONSTRAINT `fk_vdl_comment_vdl_msg1` FOREIGN KEY (`id_msg_ref`) REFERENCES `vdl_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_comment_vdl_user1` FOREIGN KEY (`id_user`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_event`
--
ALTER TABLE `vdl_event`
  ADD CONSTRAINT `fk_vdl_event_vdl_msg1` FOREIGN KEY (`id_msg`) REFERENCES `vdl_post` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_event_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_event_vdl_user1` FOREIGN KEY (`creator`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_file`
--
ALTER TABLE `vdl_file`
  ADD CONSTRAINT `fk_vdl_file_vdl_msg1` FOREIGN KEY (`id_msg`) REFERENCES `vdl_post` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_friend_of`
--
ALTER TABLE `vdl_friend_of`
  ADD CONSTRAINT `fk_vdl_friend_of_vdl_user1` FOREIGN KEY (`user1`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_friend_of_vdl_user2` FOREIGN KEY (`user2`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_group`
--
ALTER TABLE `vdl_group`
  ADD CONSTRAINT `fk_vdl_group_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_message`
--
ALTER TABLE `vdl_message`
  ADD CONSTRAINT `fk_vdl_msg_conver_vdl_conver1` FOREIGN KEY (`id_chat`) REFERENCES `vdl_chat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_msg_conver_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_notify`
--
ALTER TABLE `vdl_notify`
  ADD CONSTRAINT `fk_vdl_notify_vdl_chat1` FOREIGN KEY (`chat_id`) REFERENCES `vdl_chat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_notify_vdl_post1` FOREIGN KEY (`post_id`) REFERENCES `vdl_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_notify_vdl_user1` FOREIGN KEY (`id_creator`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_notify_vdl_user2` FOREIGN KEY (`id_receptor`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_post`
--
ALTER TABLE `vdl_post`
  ADD CONSTRAINT `fk_vdl_msg_vdl_group1` FOREIGN KEY (`id_group`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_msg_vdl_user1` FOREIGN KEY (`id_user`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_post_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_routes_open_sources`
--
ALTER TABLE `vdl_routes_open_sources`
  ADD CONSTRAINT `fk_vdl_routes_open_sources_vdl_routes_private_services1` FOREIGN KEY (`id_private_service`) REFERENCES `vdl_routes_private_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_routes_pool`
--
ALTER TABLE `vdl_routes_pool`
  ADD CONSTRAINT `fk_vdl_routes_pool_vdl_group1` FOREIGN KEY (`group_linked`) REFERENCES `vdl_group` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_routes_pool_vdl_routes_cars1` FOREIGN KEY (`car_id`) REFERENCES `vdl_routes_vehicle` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_routes_pool_vdl_routes_private_services1` FOREIGN KEY (`private_service_related`) REFERENCES `vdl_routes_private_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_routes_pool_vdl_user_routes1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_routes_private_services`
--
ALTER TABLE `vdl_routes_private_services`
  ADD CONSTRAINT `fk_vdl_routes_private_services_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_routes_stops`
--
ALTER TABLE `vdl_routes_stops`
  ADD CONSTRAINT `fk_vdl_routes_stops_vdl_user1` FOREIGN KEY (`user_related`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_routes_stops_vdl_user_routes1` FOREIGN KEY (`route_related`) REFERENCES `vdl_user_routes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_routes_trace`
--
ALTER TABLE `vdl_routes_trace`
  ADD CONSTRAINT `fk_vdl_routes_trace_vdl_user_routes1` FOREIGN KEY (`route_related`) REFERENCES `vdl_user_routes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_routes_vehicle`
--
ALTER TABLE `vdl_routes_vehicle`
  ADD CONSTRAINT `fk_vdl_routes_cars_vdl_user1` FOREIGN KEY (`id_owner`) REFERENCES `vdl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_token`
--
ALTER TABLE `vdl_token`
  ADD CONSTRAINT `fk_vdl_token_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_tracking`
--
ALTER TABLE `vdl_tracking`
  ADD CONSTRAINT `fk_vdl_tracking_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_user_data`
--
ALTER TABLE `vdl_user_data`
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group1` FOREIGN KEY (`Place`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group2` FOREIGN KEY (`group1`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group3` FOREIGN KEY (`group2`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group4` FOREIGN KEY (`group3`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group5` FOREIGN KEY (`group4`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group6` FOREIGN KEY (`group5`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_opt_vdl_group1` FOREIGN KEY (`University`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_opt_vdl_group2` FOREIGN KEY (`Work`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_user_routes`
--
ALTER TABLE `vdl_user_routes`
  ADD CONSTRAINT `fk_vdl_user_routes_vdl_routes_pool1` FOREIGN KEY (`carpool_service_related`) REFERENCES `vdl_routes_pool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_user_routes_vdl_user1` FOREIGN KEY (`id_user`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_user_routes_has_vdl_routes_open_sources`
--
ALTER TABLE `vdl_user_routes_has_vdl_routes_open_sources`
  ADD CONSTRAINT `fk_vdl_user_routes_has_vdl_routes_open_sources_vdl_routes_ope1` FOREIGN KEY (`vdl_routes_open_sources_id`) REFERENCES `vdl_routes_open_sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_user_routes_has_vdl_routes_open_sources_vdl_user_routes1` FOREIGN KEY (`vdl_user_routes_id`) REFERENCES `vdl_user_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_user_take_route`
--
ALTER TABLE `vdl_user_take_route`
  ADD CONSTRAINT `fk_vdl_user_has_vdl_user_routes_vdl_user1` FOREIGN KEY (`vdl_user_id`) REFERENCES `vdl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_user_has_vdl_user_routes_vdl_user_routes1` FOREIGN KEY (`vdl_user_routes_id`) REFERENCES `vdl_user_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_user_take_route_vdl_routes_stops1` FOREIGN KEY (`vdl_routes_stops_id`) REFERENCES `vdl_routes_stops` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_u_belong`
--
ALTER TABLE `vdl_u_belong`
  ADD CONSTRAINT `fk_vdl_u_belong_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `vdl_u_belong_ibfk_1` FOREIGN KEY (`group_name`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_u_conver`
--
ALTER TABLE `vdl_u_conver`
  ADD CONSTRAINT `fk_vdl_user_has_vdl_msg_conver_vdl_msg_conver1` FOREIGN KEY (`vdl_msg_conver_conver_ref`) REFERENCES `vdl_chat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vdl_u_conver_ibfk_1` FOREIGN KEY (`vdl_user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
